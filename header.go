//go:build unix || bsd

package goswaybarprotocol

import "syscall"

// Header type represents the swaybar header type as it is documented on
// https://github.com/swaywm/sway/blob/master/swaybar/swaybar-protocol.7.scd
type Header struct {
	// Version is required to be 1 in current swaybar version 1.7
	Version            uint `json:"version"`
	HandlesClickEvents bool `json:"click_events,omitempty"`
	StopSignal         int  `json:"stop_signal,omitempty"`
	ContinueSignal     int  `json:"cont_signal,omitempty"`
}

// MinimalHeader returns the bare minimum required swaybar protocol header
// as documented in swaybar version 1.7.
func MinimalHeader() *Header {
	return &Header{Version: 1}
}

// FullDefaultHeader returns the default swaybar protocol header values as
// documented in swaybar version 1.7 for reference and to built upon.
func FullDefaultHeader() *Header {
	return &Header{
		Version:            1,
		HandlesClickEvents: false,
		ContinueSignal:     int(syscall.SIGCONT),
		StopSignal:         int(syscall.SIGSTOP),
	}
}

// BodyField represents One Single Field inside of the protocol body, which is
// an infinite array containing arrays of fields. Thus each new array updates
// swaybar, enabling on-demand attach/detach fields and the like.
type BodyField struct {
	FullText  string `json:"full_text"`
	ShortText string `json:"short_text,omitempty"`
	IsUrgent  bool   `json:"urgent,omitempty"`
}
