package goswaybarprotocol_test

import (
	"encoding/json"
	"fmt"
	"syscall"
	"testing"

	sut "gitlab.com/meltmann/goswaybarprotocol"
)

// TestMinimalHeader against swaybar version 1.7
func TestMinimalHeader(t *testing.T) {
	got := *sut.MinimalHeader()
	want := sut.Header{Version: 1}

	if want != got {
		t.Error("output mismatch", want, got)
	}
}

// TestDefaultHeader against swaybar version 1.7
// Implementation is heavily relied on Linux systems but since swayar is for
// Linux systems this should not be an issue at all. Might differ on BSD, though,
// since this handles SIGSTOP (17), SIGSTP (18) and SIGCONT (19) differently.
//
// The go build tags work for both unix and bsd, so I test implementation here.
func TestDefaultHeader(t *testing.T) {
	got := *sut.FullDefaultHeader()
	want := sut.Header{
		Version:            1,
		HandlesClickEvents: false,
		ContinueSignal:     int(syscall.SIGCONT),
		StopSignal:         int(syscall.SIGSTOP),
	}

	if want != got {
		t.Error("output mismatch", want, got)
	}
}

// TestJsonMarshal and it's counterpart tests annotations here, thus is testing
// implementation detail. But since swaybar-protocol somewhat enforces valid
// header information this is the only known way (at least to me) verifying that
// I got the annotations right and thus assure I generate/consume valid out-/input
//
// Yes, this means as soon as json.(Un)Marshal() change their behaviours the tests
// will fail due to invisible characters. That is a drawback, but yeah, whatever.
func TestJsonMarshal(t *testing.T) {
	type sample struct {
		input *sut.Header
		want  string
	}

	cases := []sample{
		{
			input: sut.MinimalHeader(),
			want:  `{"version":1}`,
		},
		{
			input: &sut.Header{Version: 3, HandlesClickEvents: true},
			want:  `{"version":3,"click_events":true}`,
		},
		{
			input: &sut.Header{
				Version:            0,
				HandlesClickEvents: true,
				StopSignal:         19,
				ContinueSignal:     18,
			},
			want: `{"version":0,"click_events":true,"stop_signal":19,"cont_signal":18}`,
		},
	}

	for i, c := range cases {
		t.Run(
			fmt.Sprintf("Case#%d", i),
			func(t *testing.T) {
				v := c.input
				res, err := json.Marshal(v)
				if err != nil {
					t.Fatal(i, "marshal JSON", err)
				}
				got := string(res)
				if c.want != got {
					t.Error(i, "JSON mismatch", c.want, got)
				}
			},
		)
	}
}

// TestJsonUnmarshal tests annotations and documents import behaviour.
// Refer to documentation for TestJsonMarshal on implications of this fact.
func TestJsonUnmarshal(t *testing.T) {
	type sample struct {
		input string
		want  *sut.Header
	}

	cases := []sample{
		{
			want:  sut.MinimalHeader(),
			input: `{"version":1}`,
		},
		{
			want:  &sut.Header{Version: 3, HandlesClickEvents: true},
			input: `{"version":3,"click_events":true}`,
		},
		{
			want: &sut.Header{
				Version:            0,
				HandlesClickEvents: true,
				StopSignal:         19,
				ContinueSignal:     18,
			},
			input: `{"version":0,"click_events":true,"stop_signal":19,"cont_signal":18}`,
		},
		// Not sure what happens with THIS input.
		// Not even sure if that way is required at all...
		{
			want:  nil,
			input: `{"version":-1}`,
		},
	}

	for i, c := range cases {
		t.Run(
			fmt.Sprintf("Case#%d", i),
			func(t *testing.T) {
				var got sut.Header
				in := []byte(c.input)
				if err := json.Unmarshal(in, &got); err != nil {
					if c.want != nil {
						t.Fatal(i, "unmarshal JSON", c.input, err)
					}
					t.Log(i, "errored as expected", c.input, err)
					return
				}

				if c.want == nil {
					t.Fatal(i, "invalid JSON must error", c.input)
				}

				if *c.want != got {
					t.Error(i, "version mismatch", *c.want, got)
				}
			},
		)
	}
}
