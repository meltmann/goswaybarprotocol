package main

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/meltmann/goswaybarprotocol"
)

func main() {
	// Prepare Bytes
	// to generate `{"version":1}[`
	b, err := json.Marshal(goswaybarprotocol.MinimalHeader())
	if err != nil {
		panic(fmt.Errorf("marshal json: %w", err))
	}

	// Send that header over to swaybar
	// Newline is required between header and body.
	fmt.Printf("%s\n", b)

	// Send the beginning of an infinite array over to swaybar
	// and make sure to leave a safe state even if your app breaks.
	fmt.Println("[")
	defer fmt.Println("]")

	// Now you send fields. An empty row makes thinks easier for followups.
	fmt.Println(`[{"full_text":"starting engines"}]`)

	// TODO: Don't do that, somewhat handle os.Signals
	for {
		// Here we go and yes, it is useless.
		now := time.Now()
		urgent := now.Second() == 0
		f := goswaybarprotocol.BodyField{
			FullText:  now.UTC().Format(time.RFC3339),
			ShortText: now.UTC().Format("15:04"),
			IsUrgent:  urgent,
		}
		fb, err := json.Marshal(&f)
		if err != nil {
			continue
		}

		fmt.Printf(",[%s]", fb)
		time.Sleep(1 * time.Second)
	}
}
