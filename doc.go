// Package goswaybarprotocol provides types and methods for the swaybar protocol
// JSON output to make your tools shine on the swaybar.
//
// You'll find samples over at my sr.ht repository which lacks go module support
// at the moment. Feel free to use and contribute to the project as you like.
//
// To use this package first return the Header JSON to swaybar.
// For a first draft the MinimalHeader() should do.
// Then you start an infinite JSON array by reporting `[` to swaybar.
// As soon as you close this array via `]` swaybar stops updating, so make sure
// you somewhat defer it.
// Inside this array you put an array of BodyField JSON with each BodyField JSON
// representing one field in swaybar and each array is a complete swaybar update.
//
// Thus you might use it like in the sample cmd/sample/main.go

package goswaybarprotocol
